# Para correrlos, se debe realizar lo siguiente:

```sh
$ oc create -f 1_doctor_insert_job.yml
$ oc create -f 2_citizen_update_job.yml
$ oc create -f 3_citizen_insert_job.yml
$ oc create -f 4_track_insert_job.yml
$ oc create -f 5_patientDoctor_insert_job.yml
$ oc create -f 6_region_insert_job.yml
$ oc create -f 7-citizen-region-insert-job.yml
```
