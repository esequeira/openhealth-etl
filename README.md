# El job se ejecutara automatico por Cron


### Si se quiere ejecutar manualmente, se puede realizar de la siguiente manera:

```sh
cd /opt/pentaho-di/src
kitchen.sh -file=job_track_insert_openshift.kjb -level=Rowlevel
```
