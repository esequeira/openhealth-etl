FROM esequeira/pentaho:18
USER root
LABEL io.k8s.description="Platform for building and running Pentaho Data Integration applications" \
      io.k8s.display-name="Openhealth ETL" \
      io.openshift.expose-services="8080" \
      io.openshift.tags="pentaho di, etl"

RUN mkdir /opt/pentaho-di/src
ADD . /opt/pentaho-di/src
#ADD --chown=1001:1001 . /opt/pentaho-di/src
RUN chmod +x -R /opt/pentaho-di/src && chown 1001:1001 -R /opt/pentaho-di/src && rm -rf /opt/pentaho-di/src/Dockerfile
WORKDIR /opt/pentaho-di/src

#RUN chmod +x run-etl.sh  
#RUN chmod 777 -R /opt/pentaho-di/data-integration && chmod +x /opt/pentaho-di/data-integration && chown 1001:1001 /opt/pentaho-di/data-integration/carte.sh

USER 1001
RUN chmod 777 -R /opt/pentaho-di/data-integration/logs
EXPOSE 8080
